@extends('home-master')

<!-- page title -->
@section('page-title')	

@endsection


<!-- page content -->
@section('content')
<!-- home carousel starts -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner home-carousel-card">
		<div class="carousel-item home-carousel-image active">
			<img src="{{url('public/images/2.jpg')}}" class="d-block w-100" alt="...">
			<div class="carousel-caption d-none d-md-block">
				<h1>Go green city clean</h1>
				<a href="{{url('#')}}" class="sundar-btn"><p>Learn More</p></a>
			</div>
		</div>
		<div class="carousel-item home-carousel-image">
			<img src="{{url('public/images/3.jpg')}}" class="d-block w-100" alt="...">
			<div class="carousel-caption d-none d-md-block">
				<h1>Go green city clean</h1>
				<a href="{{url('#')}}" class="sundar-btn"><p>Learn More</p></a>
			</div>
		</div>
		<div class="carousel-item home-carousel-image">
			<img src="{{url('public/images/4.jpg')}}" class="d-block w-100" alt="...">
			<div class="carousel-caption d-none d-md-block">
				<h1>Go green city clean</h1>
				<a href="{{url('#')}}" class="sundar-btn"><p>Learn More</p></a>
			</div>
		</div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>





<!-- home quote -->
<div class="container-fluid padding-tb home-quote">
	<div class="container">
		<ul class="border-design">
			<li><p><span></span></p></li>
			<li><p><i class="fas fa-bus"></i></p></li>
			<li><p><span></span></p></li>
		</ul>
		<h2>"Happiness is window seat on a bus"</h2>
		<p>- random quote</p>
	</div>
</div>






<!-- home about -->
<div class="container padding-tb home-about-card">
	<ul class="border-design2">
		<li><p><span></span></p></li>
		<li><p><i class="fas fa-bus"></i></p></li>
		<li><p><span></span></p></li>
	</ul>
	<h3>Learn About us</h3>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet nibh eu euismod euismod. Suspendisse vitae pulvinar orci. Aenean sed augue nulla. Nam id aliquam lectus, vel ultrices ante. Maecenas et orci vel mauris auctor sodales vel ut orci. Quisque pellentesque dui vel justo interdum fermentum. Morbi bibendum lorem vitae turpis aliquam venenatis. Nunc sem felis, consequat at odio id, venenatis varius augue. Duis consequat enim dignissim lectus rutrum, nec rutrum ipsum rutrum. Ut pharetra finibus eros. Quisque eros lorem, sollicitudin laoreet vehicula a, pretium sit amet diam. Fusce et magna eu ligula varius dignissim quis facilisis erat. Sed a nibh dapibus felis elementum tempus.</p>

	<a href="{{url('#')}}" class="sundar-btn1"><p>Learn More</p></a>
</div>





<!-- home testimonial -->
<div class="container-fluid padding-tb home-testimonial-bg">
	<div class="container home-testimonial-card">
		<ul class="border-design">
			<li><p><span></span></p></li>
			<li><p><i class="fas fa-bus"></i></p></li>
			<li><p><span></span></p></li>
		</ul>
		<h3>Happy passenger</h3>
		<h6>We are always here to serve you</h6>
		<div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet nibh eu euismod euismod. Suspendisse vitae pulvinar orci. Aenean sed augue nulla. Nam id aliquam lectus, vel ultrices ante. Maecenas et orci vel mauris auctor sodales vel ut orci. Quisque pellentesque dui vel justo interdum fermentum. Morbi bibendum lorem vitae turpis aliquam venenatis. Nunc sem felis, consequat at odio id, venenatis varius augue.</p>
			<div class="home-testimonial-image">
				<img class="img-fluid" src="" rel="">
			</div>
			<h5>John doe</h5>
		</div>
	</div>
</div>









<!-- be a member -->
<div class="container-fluid padding-tb home-member-bg">
	<div class="container home-member-border">
		<ul class="border-design">
			<li><p><span></span></p></li>
			<li><p><i class="fas fa-bus"></i></p></li>
			<li><p><span></span></p></li>
		</ul>
	</div>
	<div class="container home-member-card">
		<div class="row"> 
			<div class="col-md-7">
				<h6>be a member with</h6>
				<h2>Sundar yatayat</h2>
				<span></span>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet nibh eu euismod euismod. Suspendisse vitae pulvinar orci. Aenean sed augue nulla. Nam id aliquam lectus, vel ultrices ante. Maecenas et orci vel mauris auctor sodales vel ut orci. Quisque pellentesque dui vel justo interdum fermentum. Morbi bibendum lorem vitae turpis aliquam venenatis. Nunc sem felis, consequat at odio id, venenatis varius augue.</p>
				<div class="container-fluid home-membership-card">
					<div class="row">
						<div class="col-md-4">
							<h4>Silver</h4>
							<p>$30</p>
							<p>$120</p>
						</div>
						<div class="col-md-4 home-membership-border">
							<h4>Gold</h4>
							<p>$30</p>
							<p>$120</p>
						</div>
						<div class="col-md-4">
							<h4>Premium</h4>
							<p>$30</p>
							<p>$120</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">

			</div>
		</div>
	</div>
</div>









<!-- home app section  -->
<div class="container-fluid padding-tb">
	<div class="row">
		<div class="col-md-6">
			
		</div>
		<div class="col-md-6">
			<h1>Sundar Yatayat</h1>
			<h3>App on the market, get it now</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet nibh eu euismod euismod. Suspendisse vitae pulvinar orci. Aenean sed augue nulla. Nam id aliquam lectus, vel ultrices ante. Maecenas et orci vel mauris auctor sodales vel ut orci. Quisque pellentesque dui vel justo interdum fermentum. Morbi bibendum lorem vitae turpis aliquam venenatis. Nunc sem felis, consequat at odio id, venenatis varius augue.</p>
		</div>
	</div>
</div>
@endsection