<div class="container-fluid footer-bg padding-tb">
	<div class="container">
		<div class="row">
			<div class="col-md-3 footer-card">
				<h3>Sundar Yatayat</h3>
				<div class="footer-logo">
					<img src="{{url('public/images/logo.png')}}" class="img-fluid" rel="logo of sundar yatayat">
				</div>
			</div>
			<div class="col-md-3 footer-card">
				<h5>Services</h5>
				<ul>
					<li><a href="{{url('#')}}"><p>Service One</p></a></li>
					<li><a href="{{url('#')}}"><p>Service Two</p></a></li>
					<li><a href="{{url('#')}}"><p>Service Three</p></a></li>
				</ul>
			</div>
			<div class="col-md-3 footer-card">
				<h5>Links</h5>
				<ul>
					<li><a href="{{url('#')}}"><p>Home</p></a></li>
					<li><a href="{{url('#')}}"><p>About</p></a></li>
					<li><a href="{{url('#')}}"><p>Route</p></a></li>
					<li><a href="{{url('#')}}"><p>Smart Card</p></a></li>
				</ul>
			</div>
			<div class="col-md-3 footer-card">
				<h5>Contact Us</h5>
				<ul>
					<li><a href="{{url('#')}}"><p>Address goes here</p></a></li>
					<li><a href="{{url('#')}}"><p>Phone goes here</p></a></li>
					<li><a href="{{url('#')}}"><p>Email goes here</p></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p>Copyright {{date("Y")}}, &copy;Sundar Yatayat, All rights reserved.</p>
			</div>
			<div class="col-md-6">
				<p>Designed and Developed by: <a href="">Creatu Developers</a></p>
			</div>
		</div>
	</div>
</div>