
<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light navbar-bg">
  <a class="navbar-brand navbar-logo" href="#">
    <img src="{{url('public/images/logo.png')}}" class="img-fluid" rel="logo of sundar yatayat">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse navbar-menu" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Route</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Smart Card</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Services
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Services One</a>
          <a class="dropdown-item" href="#">Services Two</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">News</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Events</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Gallery
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Photos</a>
          <a class="dropdown-item" href="#">Videos</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
      </li>
    </ul>
  </div>
</nav>

<div class="social-wrapper">
  <ul>
    <li class="facebook">
      <i class="fab fa-facebook-f" aria-hidden="true"></i>
      <div class="slider">
        <p>facebook</p>
      </div>
    </li>
    <li class="twitter">
      <i class="fab fa-twitter" aria-hidden="true"></i>
      <div class="slider">
        <p>twitter</p>
      </div>
    </li>
    <li class="instagram">
      <i class="fab fa-instagram" aria-hidden="true"></i>
      <div class="slider">
        <p>instagram</p>
      </div>
    </li>
    <li class="google">
      <i class="fab fa-google-plus-g" aria-hidden="true"></i>
      <div class="slider">
        <p>google</p>
      </div>
    </li>
    <li class="whatsapp">
      <i class="fab fa-whatsapp" aria-hidden="true"></i>
      <div class="slider">
        <p>whatsapp</p>
      </div>
    </li>
  </ul>
</div>


<!-- facebook -->
<div class="fb-livechat">
  <div class="ctrlq fb-overlay"></div>
  <div class="fb-widget">
    <div class="ctrlq fb-close"></div>
    <div class="fb-page" data-href="https://www.facebook.com/pagename/" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false">
      <blockquote cite="https://www.facebook.com/pagename/" class="fb-xfbml-parse-ignore"> </blockquote>
    </div>
    <div class="fb-credit"> 
      <a href="https://www.facebook.com/pagename/" target="_blank">Facebook Chat</a>
    </div>
    <div id="fb-root"></div>
  </div>
  <a href="https://www.facebook.com/pagename/" title="Send us a message on Facebook" class="ctrlq fb-button"></a> 
</div> 